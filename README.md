


# Downloading Image

For latest image:
```
curl -O https://research-containers-01.oit.duke.edu/POMMS/pomms-data-image_latest.sif
```

For tagged image: `curl -O https://research-containers-01.oit.duke.edu/POMMS/pomms-data-image_latest.sif`

for example:
```
curl -O https://research-containers-01.oit.duke.edu/POMMS/pomms-data-image_v000.sif
```

# References
The Gitlab CI YML used here is tweaked from Mike Newton's example here: https://gitlab.oit.duke.edu/OIT-DCC/apptainer-example/
